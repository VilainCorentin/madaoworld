import {Construction} from "@/data/Construction";

let SolarPanelCreated = 0;

export class SolarPanel extends Construction{

    constructor(){
        super();
        this.id = SolarPanelCreated;
        this.name = "Pedro Motoa"
        this.xp = 0;
        this.maxOccupancy = 1;
        SolarPanelCreated++;
    }

    electricProduction(){
        return this.occupants.length * 10;
    }

}