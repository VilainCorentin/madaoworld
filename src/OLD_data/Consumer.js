let consumerCreated = 0;

export class Consumer {

    constructor(attributes){
        this.id = consumerCreated;
        this.selectCharge = attributes.selectCharge || null;
        this.consoMax = attributes.consoMax || null;
        this.actualConso = attributes.actualConso || null;
        consumerCreated++;
    }

    expectedConso(){
        return ((this.selectCharge * this.consoMax) / 100)
    }

    static nbConsumerCreated(){
        return consumerCreated;
    }
}