import {Construction} from "@/data/Construction";

let makeshiftTeneCreated = 0;

export class MakeshiftTent extends Construction{

    constructor(){
        super();
        this.id = makeshiftTeneCreated;
        this.name = "El tento de mama"
        this.xp = 0;
        this.maxOccupancy = 10;
        makeshiftTeneCreated++;
    }

}