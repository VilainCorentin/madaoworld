import {Construction} from "@/data/Construction";

let ExtractorCreated = 0;

export class EarthExtractor extends Construction{

    constructor(){
        super();
        this.id = ExtractorCreated;
        this.name = "Pedro Motoa"
        this.xp = 0;
        this.maxOccupancy = 25;
        this.selectCharge = 0;
        this.consoMax = 20;
        this.actualConso = 0;

        ExtractorCreated++;
    }

    expectedConso(){
        return ((this.selectCharge * this.consoMax) / 100)
    }

    mokaProduction(){
        return Math.round(this.occupants.length * (this.actualConso / this.consoMax));
    }

}