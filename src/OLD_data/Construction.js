export class Construction {
    constructor() {
        this.occupants = [];
        this.maxOccupancy = 0;
        this.level = 1;
    }

    isFull(){
        return ((this.currentOccupancy + 1) > this.maxOccupancy)
    }

    addOccupant(autochthon) {
        this.occupants.push(autochthon);
    }

    removeOccupant(autochthon) {
        this.occupants = this.occupants.filter(item => item !== autochthon);
    }

    get currentOccupancy() {
        return this.occupants.length;
    }
}