import axios from 'axios'

const api_url = 'http://localhost:3000/user'

const state = {
    status: '',
    token: localStorage.getItem('token') || '',
    user: {}
}

const mutations = {
    request(state) {
        state.status = 'loading'
    },
    auth_success(state, token) {
        state.status = 'success'
        state.token = token
    },
    register_success(state, user) {
        state.status = 'success'
        state.user = user
    },
    error(state) {
        state.status = 'error'
    },
    logout(state) {
        state.status = ''
        state.token = ''
    },
}

const actions = {
    login({
              commit
          }, user) {
        return new Promise((resolve, reject) => {
            commit('request')
            axios({
                url: `${api_url}/authenticate`,
                data: user,
                method: 'POST'
            })
                .then(resp => {
                    const token = resp.data.token
                    localStorage.setItem('token', token) // Le jeton doit être stocké dans le localStorage et pas dans le store (vide si quitte)
                    axios.defaults.headers.common['Authorization'] = token
                    commit('auth_success', token) // Permet de déclencher une mutation
                    resolve(resp)
                })
                .catch(err => {
                    commit('error')
                    localStorage.removeItem('token')
                    reject(err.response.data.message)
                })
        })
    },
    register({
                 commit
             }, user) {
        return new Promise((resolve, reject) => {
            commit('request')
            axios({
                url: `${api_url}/register`,
                data: user,
                method: 'POST'
            })
                .then(resp => {
                    const user = resp.data.user
                    commit('register_success', user)
                    resolve(resp)
                })
                .catch(err => {
                    commit('error', err)
                    reject(err.response.data.message)
                })
        })
    },
    logout({
               commit
           }) {
        return new Promise((resolve) => {
            commit('logout')
            localStorage.removeItem('token')
            delete axios.defaults.headers.common['Authorization']
            resolve()
        })
    }
}

const getters = {
    isLoggedIn: state => !!state.token,
    authStatus: state => state.status,
}

export default {
    state,
    mutations,
    actions,
    getters
};
