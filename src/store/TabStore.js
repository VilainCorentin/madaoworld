
const state = {
    TABS: [
        {
            title: 'Supervision',
            value: 'Supervision',
        },
        {
            title: 'Laboratoire',
            value: 'Laboratoire',
        },
        {
            title: 'Population',
            value: 'Population',
        },
    ]
}

const mutations = {
    addTab (state, title){
        if(state.TABS.map(function(e) { return e.title; }).indexOf(title) === -1){
            state.TABS.push({
                title: title,
                value: title
            })
        }
    }
}

const getters = {
    getTabs: state => {
        return state.TABS
    },
}

export default {
    state,
    mutations,
    getters
};