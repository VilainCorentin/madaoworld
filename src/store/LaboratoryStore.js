import {ExperimentalLaboratory} from "../data/ship/moduleShip/ExperimentalLaboratory";
import PlanDataTrigger from "../data/ship/improvement/plans/PlanDataTrigger";

const state = {
    Laboratory: new ExperimentalLaboratory(),
};

const actions = {
    unlockElemInPlan({commit, state}, {indPlan, category, indImpr, amount, idFunc}){
        state.Laboratory.unlockElemInPlan(indPlan, category, indImpr)
        for(let id in amount){
            commit('decrementResource', {type: amount[id].type, name: amount[id].name, amount: amount[id].amount})
        }
        if(PlanDataTrigger.functionForPlan[idFunc] !== undefined){
            PlanDataTrigger.functionForPlan[idFunc]()
        }
    },
};

const mutations = {
    unlockPlan (state, indPlan){
        state.Laboratory.unlockPlan(indPlan)
    },
};

const getters = {
    getLaboratory: state => {
        return state.Laboratory
    },
    getNextPlanAvailable: state => {
        let index = state.Laboratory.listPlan.map(function(e) { return e.unlock; }).indexOf(0)
        if(index === -1){
            return null
        }
        let plan = state.Laboratory.listPlan[index]
        plan['id'] = index
        return plan
    },
};

export default {
    state,
    mutations,
    getters,
    actions
};