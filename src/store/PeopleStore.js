
const state = {
    autochthons: [],
}

const mutations = {
    addAutochthon (state, a){
        state.autochthons.push(a)
    },
    removeAutochthon (state, selected){
        let index = state.autochthons.map(function(e) { return e.id; }).indexOf(selected.id)
        state.autochthons.splice(index, 1)
    }
}

const getters = {
    getAutochthons: state => {
        return state.autochthons
    },
}

export default {
    state,
    mutations,
    getters
};