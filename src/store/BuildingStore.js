import {ConstructionStorage} from "../data/construction/ConstructionStorage";
import {GraphicRepresentationJS} from "../PixiJS/GraphicRepresentationJS";

const state = {
    buildingState: new ConstructionStorage
}

const mutations = {
    addBuilding (state, {type, num, build}){
        GraphicRepresentationJS.addBuilding(type);
        state.buildingState.addBuilding(type, num, build)
    }
}

const getters = {
    getBuildingStateByType: state => (type, num) => {
        return state.buildingState.getBuildingsByType(type, num)
    },
    getBuildingState: state => type => {
        return state.buildingState.getConstruction(type)
    },
    getAllBuilding: state => {
        return state.buildingState.constructStorage
    }
}

export default {
    state,
    mutations,
    getters
};