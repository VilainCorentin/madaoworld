import Vue from 'vue';
import Vuex from 'vuex';

import tabs from './TabStore'
import resources from './ResourceStore'
import laboratory from './LaboratoryStore'
import constructions from './ConstructionStore'
import building from './BuildingStore'
import terminal from './TerminalStore'
import people from './PeopleStore'
import User from './UsersStore'

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        tabs,
        resources,
        laboratory,
        constructions,
        building,
        terminal,
        people,
        User
    }
});