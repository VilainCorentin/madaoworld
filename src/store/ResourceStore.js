import {BindingResources} from "../data/ressources/BindingResources";

const state = {
    Resources: new BindingResources()
};

const mutations = {
    incrementMoka (state, n){
        state.Resources.addAmountResource('essential', 'moka', n)
    },
    incrementResource(state, {type, name, amount}){
        state.Resources.addAmountResource(type, name, amount)
    },
    decrementResource(state, {type, name, amount}){
        state.Resources.removeAmountResource(type, name, amount)
    },
    addKindOfResource(state, {type, name}){
        state.Resources.addKindOfResource(type, name)
    }
};

const getters = {
    getBindingResources: state => {
        return state.Resources
    },
    getBindingResourcesList: state => {
        return state.Resources.bindingResource
    },
    getResourceByTypeAndName: state => (type, name) => {
            return state.Resources.getAmountResource(type, name)
    }
};

export default {
    state,
    mutations,
    getters
};