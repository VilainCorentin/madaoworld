import {BinaryAlternator} from "../data/ship/moduleShip/BinaryAlternator";
import {FoodTransponder} from "../data/ship/moduleShip/FoodTransponder";

const state = {
    terminal: {
        alternator: new BinaryAlternator(),
        transpondeur: new FoodTransponder()
    }
};

const actions = {

};

const mutations = {
    toggleActivation(state, {val, id}){
        state.terminal[id].toggleActivation(val)
    }
};

const getters = {
    getAlternator: state => {
        return state.terminal['alternator']
    },
    getTranspondeur: state => {
        return state.terminal['transpondeur']
    },
    getTerminal: state => {
        return state.terminal
    }
};

export default {
    state,
    mutations,
    getters,
    actions
};