
const state = {
    constructionState: []
}

const mutations = {
    addConstructionState (state, construction){
        state.constructionState.push(construction)
    }
}

const getters = {
    getConstructionState: state => {
        return state.constructionState
    }
}

export default {
    state,
    mutations,
    getters
};