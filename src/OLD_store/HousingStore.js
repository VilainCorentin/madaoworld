
const state = {
    housingModule: [
        {id: 1, title: "Makeshift Tent", component: "MakeshiftTent", listOfCreated: []},
    ]
}

const mutations = {
    addHousingCreatedToList(state, {id, item}){
        let index = state.housingModule.map(function(e) { return e.id; }).indexOf(id)
        state.housingModule[index].listOfCreated.push(item)
    },
    removeHousingCreatedToList(state, {id, item}){
        let index = state.housingModule.map(function(e) { return e.id; }).indexOf(id)
        let indexItem = state.housingModule[index].listOfCreated.map(function(e) { return e.id; }).indexOf(item.id)
        state.housingModule[index].listOfCreated.splice(indexItem, 1)
    }
}

const getters = {
    getHousingModule: state => {
        return state.housingModule
    },
    getHousingListOfCreated: state => id => {
        let index = state.housingModule.map(function(e) { return e.id; }).indexOf(id)
        return state.housingModule[index].listOfCreated
    }
}

export default {
    state,
    mutations,
    getters
};