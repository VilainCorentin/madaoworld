
const state = {
    shipState: [
        { title: 'Nutritional Paste Filler', flex: 12, fonction: 'NPF', requireMoka: 5, isDone: false },
        { title: 'Quantum Generator', flex: 12, fonction: 'QG', requireMoka: 5, isDone: false },
        { title: 'Backup Alternator', flex: 12, fonction: 'BA', requireMoka: 5, isDone: false },
        { title: 'Construction Workshop', flex: 12, fonction: 'CW', requireMoka: 5, isDone: false },
        { title: 'Automaton Assembler', flex: 12, fonction: 'AA', requireMoka: 5, isDone: false },
    ]
}

const mutations = {
    setModuleDone (state, selected){
        let index = state.shipState.map(function(e) { return e.fonction; }).indexOf(selected)
        if(index !== -1){
            state.shipState[index].isDone = true
        }
    }
}

const getters = {
    getShipState: state => {
        return state.shipState
    },
    shipModuleIsDone: state => selected => {
        let index = state.shipState.map(function(e) { return e.fonction; }).indexOf(selected)
        return state.shipState[index].isDone
    }
}

export default {
    state,
    mutations,
    getters
};