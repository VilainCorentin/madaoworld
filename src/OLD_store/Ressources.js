
const state = {
    moka: 0,
    nutrivePate: 0,
    eletricMax: 0,
    electicCurrent: 0,
    quantiumFluid : 0
}

const mutations = {
    incrementMoka (state, n){
        state.moka += n
    },
    decreaseMoka (state, n){
        state.moka -= n
    },
    incrementPn (state, n){
        state.nutrivePate += n
    },
    SetElecMax (state, n){
        state.eletricMax = n
    },
    setElectricConso (state, n){
        state.electicCurrent = state.eletricMax - n
    },
    incrementQFluid (state, n){
        state.quantiumFluid += n
    }
}

const getters = {
    mokas: state => {
        return state.moka
    },
    nutritivePates: state => {
        return state.nutrivePate
    },
    maxEletrics: state => {
        return state.eletricMax
    },
    currentEletrics: state => {
        return state.electicCurrent
    },
    quantiumFluids: state => {
        return state.quantiumFluid
    }
}

export default {
    state,
    mutations,
    getters
};