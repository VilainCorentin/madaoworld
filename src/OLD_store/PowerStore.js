
const state = {
    powerModule: []
}

const mutations = {
    addPowerCreatedToList(state, {id, item}){
        let index = state.powerModule.map(function(e) { return e.id; }).indexOf(id)
        state.powerModule[index].listOfCreated.push(item)
    },
    removePowerCreatedToList(state, {id, item}){
        let index = state.powerModule.map(function(e) { return e.id; }).indexOf(id)
        let indexItem = state.powerModule[index].listOfCreated.map(function(e) { return e.id; }).indexOf(item.id)
        state.powerModule[index].listOfCreated.splice(indexItem, 1)
    },
    addPowerModule(state, module){
        state.powerModule.push(module)
    }
}

const getters = {
    getPowerModule: state => {
        return state.powerModule
    },
    getPowerListOfCreated: state => id => {
        let index = state.powerModule.map(function(e) { return e.id; }).indexOf(id)
        return state.powerModule[index].listOfCreated
    }
}

export default {
    state,
    mutations,
    getters
};