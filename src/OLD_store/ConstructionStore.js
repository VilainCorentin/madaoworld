
const state = {
    constructionState: [
        {title: "Exploitation", icon: "landscape", composants: ["Exploitations"]},
        {title: "Housing", icon: "location_city", composants: ["Housing"]},
        {title: "Power", icon: "flash_on", composants: ["Power"]}
    ]
}

const mutations = {
    addConstructionState (state, construction){
        state.constructionState.push(construction)
    }
}

const getters = {
    getConstructionState: state => {
        return state.constructionState
    }
}

export default {
    state,
    mutations,
    getters
};