import * as PIXI from 'pixi.js';

const WINDOW_WIDTH = 800;
const WINDOW_HEIGHT = 600;
const SPACESHIP_WIDTH = ((WINDOW_WIDTH / 3.2) | 0);
const SPACESHIP_Y = ((WINDOW_HEIGHT / 2.4) | 0);
const BUILDING_WIDTH = ((WINDOW_WIDTH / 6.4) | 0);
const BUILDING_HEIGHT = ((WINDOW_HEIGHT / 4.8) | 0);
const NEW_BUILDING_WIDTH = ((WINDOW_WIDTH / 18) | 0);
const NEW_BUILDING_HEIGHT = ((WINDOW_HEIGHT / 16) | 0);
const RIGHT_X = ((WINDOW_WIDTH / 1.34) | 0);
const LEFT_X = ((WINDOW_WIDTH / 16) | 0);
const TOP_Y = ((WINDOW_HEIGHT / 12) | 0);
const BOTTOM_Y = ((WINDOW_HEIGHT / 1.4) | 0);
const VITAL_COLOR = 0xE389AE;
const HABITATION_COLOR = 0xC64321;
const CHIMICAL_COLOR = 0xAB05E3;
const ENERGY_COLOR = 0x33C6BC;
const MINERAL_COLOR = 0x8F8F8B;
const ANIMAL_COLOR = 0x7a2903;
const METAL_COLOR = 0x2A3941;
const FACTORY_COLOR = 0x3E3E3C;

// const NB_MADAO = 10;

export let GraphicRepresentationJS = {
    //Aliases
    Application: PIXI.Application,
    loader: PIXI.loader,
    resources: PIXI.loader.resources,
    Sprite: PIXI.Sprite,
    madaoList: [],

    //Create a Pixi Application
    PixiApp: new PIXI.Application({
            width: WINDOW_WIDTH,
            height: WINDOW_HEIGHT,
            antialias: true,
            backgroundColor: 0x78ba2c,
            transparent: false,
            resolution: 1
        }
    ),

    init() {
        //Add the canvas that Pixi automatically created for you to the HTML document
        document.getElementById('game').appendChild(this.PixiApp.view);
        //load an image and run the `setup` function when it's done
        this.loader.load(this.setup());
    },

    getRandomBetween(min, max) {
        return Math.round(Math.random() * (max - min) + min);
    },

    createRectangle(color, x ,y, width, height, ) {
        let rectangle = new PIXI.Graphics();
        rectangle.beginFill(color);
        rectangle.drawRect(x, y, width, height);
        rectangle.endFill();
        return rectangle;
    },

    createAutochtone() {
        return this.createRectangle(0x0de0ce, this.getRandomBetween(0, WINDOW_WIDTH), this.getRandomBetween(0, WINDOW_HEIGHT), 10, 10);
    },

    createBuilding(color, x, y, width, height) {
        GraphicRepresentationJS.PixiApp.stage.addChild(this.createRectangle(color,x,y,width,height));
    },

    createBuildingWithPicture(color, x, y, width, height, picturePath) {
        GraphicRepresentationJS.PixiApp.stage.addChild(this.createRectangle(color,x,y,width,height));
        const sprite = GraphicRepresentationJS.Sprite.fromImage(picturePath);
        sprite.x = Math.round((width / 2) + x);
        sprite.y = Math.round((height / 2) + y);
        sprite.anchor.set(0);
        GraphicRepresentationJS.PixiApp.stage.addChild(sprite);
    },

    addMadao() {
        const madao = this.createAutochtone();
        madao.path = this.getRandomBetween(1, 4);
        GraphicRepresentationJS.PixiApp.stage.addChild(madao);
        this.madaoList.push(madao);
        GraphicRepresentationJS.PixiApp.ticker.add(() => this.madaoMovement(madao));
    },

    removeMadao() {
        const madao = this.madaoList.pop();
        GraphicRepresentationJS.PixiApp.stage.removeChild(madao);
    },

    addBuilding(buildingType) {
        // console.log(buildingType);
        let currentColor;
        switch(buildingType) {
            case 'Vitale':
                currentColor = VITAL_COLOR;
                break;
            case 'Habitat':
                currentColor = HABITATION_COLOR;
                break;
            case 'Chimique':
                currentColor = CHIMICAL_COLOR;
                break;
            case 'Electricity':
                currentColor = ENERGY_COLOR;
                break;
            case 'Animal':
                currentColor = ANIMAL_COLOR;
                break;
            case 'Mineral':
                currentColor = MINERAL_COLOR;
                break;
            case 'Alliage':
                currentColor = METAL_COLOR;
                break;
            case 'Usinage':
                currentColor = FACTORY_COLOR;
                break;
        }
        const x = this.getRandomBetween(0, WINDOW_WIDTH - NEW_BUILDING_WIDTH);
        const y = this.getRandomBetween(0, WINDOW_HEIGHT - NEW_BUILDING_HEIGHT);
        this.createBuilding(currentColor, x, y, NEW_BUILDING_WIDTH, NEW_BUILDING_HEIGHT);
    },

    //This `setup` function will run when the image has loaded
    setup() {
        this.createBuildingWithPicture(0x9eafae,SPACESHIP_WIDTH,SPACESHIP_Y,SPACESHIP_WIDTH,BUILDING_HEIGHT,'./../../assets/PixiPictures/start.png');
        /* this.createBuilding(0xc4173c,RIGHT_X,TOP_Y,BUILDING_WIDTH,BUILDING_HEIGHT,'./../../assets/PixiPictures/factory.png');
        this.createBuilding(0xf2d413,LEFT_X,TOP_Y,BUILDING_WIDTH,BUILDING_HEIGHT,'./../../assets/PixiPictures/fork.png');
        this.createBuilding(0x2196f3,LEFT_X,BOTTOM_Y,BUILDING_WIDTH,BUILDING_HEIGHT,'./../../assets/PixiPictures/microscope.png');
        this.createBuilding(0x8A2BE2,RIGHT_X,BOTTOM_Y,BUILDING_WIDTH,BUILDING_HEIGHT,'./../../assets/PixiPictures/microscope.png'); */
    },

    madaoMovement(madao) {
        switch (madao.path) {
            case 0:
                this.moveToSpaceship(madao);
                break;
            case 1:
                this.moveToCafeteria(madao);
                break;
            case 2:
                this.moveToFactory(madao);
                break;
            case 3:
                this.moveToLaboratory(madao);
                break;
            case 4:
                this.moveToWarehouse(madao);
                break;
        }
    },

    moveToFactory(element){
        this.moveToGivenObject(element, RIGHT_X, TOP_Y, BUILDING_WIDTH, BUILDING_HEIGHT);
    },

    moveToWarehouse(element){
        this.moveToGivenObject(element, RIGHT_X,BOTTOM_Y,BUILDING_WIDTH,BUILDING_HEIGHT,);
    },

    moveToLaboratory(element){
        this.moveToGivenObject(element, LEFT_X, BOTTOM_Y, BUILDING_WIDTH, BUILDING_HEIGHT);
    },

    moveToCafeteria(element){
        this.moveToGivenObject(element, LEFT_X, TOP_Y, BUILDING_WIDTH, BUILDING_HEIGHT);
    },

    moveToSpaceship(element){
        this.moveToGivenObject(element, SPACESHIP_WIDTH, SPACESHIP_Y, SPACESHIP_WIDTH, BUILDING_HEIGHT);
    },

    moveToGivenObject(element, x, y, width, height) {
        const middleXObject = Math.round((width / 2) + x);
        const middleYObject = Math.round((height / 2) + y);
        const posX = element.graphicsData[0].shape.x + element.x;
        const posY = element.graphicsData[0].shape.y + element.y;
        if (posX === middleXObject && posY === middleYObject) {
            if (element.path === 0) {
                element.path = this.getRandomBetween(1,4);
            } else {
                element.path = this.getRandomBetween(0,4);
            }
        } else {
            if (posX < middleXObject) {
                element.x++;
            } else if (posX > middleXObject) {
                element.x--;
            }
            if (posY < middleYObject) {
                element.y++;
            } else if (posY > middleYObject) {
                element.y--;
            }
        }
    }
};