import Vue from 'vue'
import App from './App.vue'
import LodashForVue from 'lodash-for-vue'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import Snotify from 'vue-snotify'
import store from './store/indexStore'
import { router } from './router'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import Axios from 'axios'

library.add(faTimes)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(Vuetify, {})
Vue.use(LodashForVue)

const options = {
  toast: {
    position: "rightBottom"
  }
}

Vue.use(Snotify, options)

Vue.config.productionTip = false

window.bus = new Vue()

Vue.prototype.$http = Axios; // Permet d'utiliser this.$http plutôt que axios

const token = localStorage.getItem('user-token')

if (token) {
  Vue.prototype.$http.defaults.headers.common['Authorization'] = token // Set le header avec le token de l'utilisateur pour chaque requête
}

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
