import {Construction} from "./Construction";
import store from "../../store/indexStore"

export class ConstructionResource extends Construction{

    constructor(attr){
        super(attr);
        this.production = attr.production || 1;
        this.resourceCreated = attr.resourceCreated;
    }

    getProduction(){
        //TODO PRENDRE EN COMPTE LES WORKERS

        let amount = 0

        if(store.getters.getResourceByTypeAndName('essential', 'nutritious_paste') >= this.occupants.length){
            let conso = 1
            if(this.consumption !== 0){
                conso = this.actualConso / this.consumption
            }
            amount = Math.round(this.production * this.occupants.length * conso)
            this.actualProduction = amount
            store.commit('incrementResource', {type: this.resourceCreated.type, name: this.resourceCreated.name, amount: amount})
            store.commit('decrementResource', {type: 'essential', name: 'nutritious_paste', amount: this.occupants.length})
        }
    }

}