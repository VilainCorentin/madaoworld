let constructionData = {
    0: {
        name: "Feu de camp électronique",
        cost: [
            {type: 'essential', name: 'moka', amount: 10},
            ],
        maxOccupancy: 5,
        levelBonus: 1,
        consumption: 1,
        description: "Entassement de petites leds chauffantes alimenté par du moka capable de réchauffer le coeur d’un travailleur valeureux ou d’un sans abri nécessiteux.",
    },
    1: {
        name: "Tente de fortune chauffée",
        cost: [
            {type: 'essential', name: 'moka', amount: 10},
            {type: 'wildResource', name: 'wood', amount: 10},
            {type: 'chemicalSubstance', name: 'propane', amount: 10},
            ],
        maxOccupancy: 15,
        consumption: 2,
        levelBonus: 1,
        bonus: 2,
        description: "C’est pas le Pérou mais les occupants seront au chaud et plus ou moins à l’abri.",
    },
};

export default {
    constructionData
};