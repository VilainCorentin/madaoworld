import {Construction} from "./Construction";
import store from "../../store/indexStore"
import {ConstructionResource} from "./ConstructionResource";

export class ConstructionCraft extends ConstructionResource{

    constructor(attr){
        super(attr);
        this.resourceUsed = attr.resourceUsed || [];
    }

    getProduction(){

        let prod = 0

        //TODO PRENDRE EN COMPTE LES WORKERS
        if(store.getters.getResourceByTypeAndName('essential', 'nutritious_paste') >= this.occupants.length){
            let conso = 1
            if(this.consumption !== 0){
                conso = this.actualConso / this.consumption
            }

            for(let res in this.resourceUsed){
                if(store.getters.getResourceByTypeAndName(this.resourceUsed[res].type, this.resourceUsed[res].name) < this.resourceUsed[res].amount){
                    return
                }
            }

            for(let res in this.resourceUsed){
                store.commit('decrementResource', {type: this.resourceUsed[res].type, name: this.resourceUsed[res].name, amount: this.resourceUsed[res].amount})
            }

            prod = Math.round(this.production * this.occupants.length * conso)
            this.actualProduction = prod

            store.commit('incrementResource', {type: this.resourceCreated.type, name: this.resourceCreated.name, amount: prod})
            store.commit('decrementResource', {type: 'essential', name: 'nutritious_paste', amount: this.occupants.length})
        }
    }

}