let constructionData = {
    0: {
        name: "Tube de plantation",
        cost: [
            {type: 'essential', name: 'moka', amount: 10},
            {type: 'mineral', name: 'iron', amount: 10},
            {type: 'chemicalSubstance', name: 'propane', amount: 10},
        ],
        maxOccupancy: 1,
        levelBonus: 1,
        consumption: 5,
        production: 1,
        resourceCreated: {type: 'wildResource', name: 'wood'},
        description: "Tube métallique hermétique suralimenté au propane pour obtenir du bois bien dur. "
    },
    1: {
        name: "Cuve de culture cellulaire primaire",
        cost: [
            {type: 'essential', name: 'moka', amount: 10},
            {type: 'wildResource', name: 'wood', amount: 10},
            {type: 'chemicalSubstance', name: 'methane', amount: 10},
        ],
        maxOccupancy: 3,
        levelBonus: 1,
        consumption: 3,
        production: 0.5,
        condition: {plan: 1, type: 'construction', num: 5},
        resourceCreated: {type: 'chemicalSubstance', name: 'leather'},
        description: "Exploiter les cellules souches est très mal vu depuis le LXXXIIIe siècle."
    },
};

export default {
    constructionData
};