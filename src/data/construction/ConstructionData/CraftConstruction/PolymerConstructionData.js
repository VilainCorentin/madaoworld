let constructionData = {
    0: {
        name: "Presse à granulé polycarbonaté",
        cost: [
            {type: 'essential', name: 'moka', amount: 10},
            {type: 'mineral', name: 'iron', amount: 10},
            {type: 'alloy', name: 'steel', amount: 10},
        ],
        maxOccupancy: 8,
        levelBonus: 1,
        consumption: 5,
        condition: {plan: 1, type: 'construction', num: 6},
        resourceUsed: [
            {type: 'chemicalSubstance', name: 'methane', amount: 3},
            {type: 'mineral', name: 'carbon', amount: 3},
        ],
        production: 1,
        resourceCreated: {type: 'polymer', name: 'polycarbonate'},
        description: "Faite juste attention de pas faire des biberons avec ce que vous obtiendrez."
    },
    1: {
        name: "Laminoir gravitationnel",
        cost: [
            {type: 'essential', name: 'moka', amount: 10},
            {type: 'wildResource', name: 'leather', amount: 10},
            {type: 'wildResource', name: 'wood', amount: 10},
            {type: 'mineral', name: 'iron', amount: 10},
        ],
        maxOccupancy: 10,
        levelBonus: 1,
        consumption: 15,
        resourceUsed: [
            {type: 'chemicalSubstance', name: 'water', amount: 1},
            {type: 'wildResource', name: 'leather', amount: 3},
            {type: 'chemicalSubstance', name: 'leather', amount: 2},
        ],
        production: 1,
        condition: {plan: 1, type: 'construction', num: 7},
        resourceCreated: {type: 'polymer', name: 'rubber'},
        description: "En aplatissant n’importe quoi avec la force gravitationnel d’un trou noir, vous obtiendrez du caoutchouc… science bitch."
    },
    2: {
        name: "Mousseuse haute pression",
        cost: [
            {type: 'essential', name: 'moka', amount: 10},
            {type: 'mineral', name: 'iron', amount: 10},
            {type: 'polymer', name: 'polycarbonate', amount: 10},
        ],
        maxOccupancy: 6,
        levelBonus: 1,
        consumption: 18,
        resourceUsed: [
            {type: 'chemicalSubstance', name: 'methane', amount: 4},
            {type: 'chemicalSubstance', name: 'propane', amount: 2},
        ],
        production: 1,
        condition: {plan: 1, type: 'construction', num: 8},
        resourceCreated: {type: 'polymer', name: 'polyester'},
        description: "Et que ça mousse aussi vos collègues."
    },
    3: {
        name: "Polymérisateur de colle",
        cost: [
            {type: 'essential', name: 'moka', amount: 10},
            {type: 'polymer', name: 'rubber', amount: 10},
            {type: 'alloy', name: 'steel', amount: 10},
        ],
        maxOccupancy: 3,
        levelBonus: 1,
        consumption: 8,
        resourceUsed: [
            {type: 'chemicalSubstance', name: 'methane', amount: 2},
            {type: 'polymer', name: 'rubber', amount: 1},
            {type: 'chemicalSubstance', name: 'water', amount: 3},
        ],
        production: 0.25,
        condition: {plan: 1, type: 'construction', num: 9},
        resourceCreated: {type: 'polymer', name: 'glue'},
        description: "Et que ça mousse aussi vos collègues."
    },
    4: {
        name: "Injecteur à bi-mélange pour adhésif",
        cost: [
            {type: 'essential', name: 'moka', amount: 10},
            {type: 'wildResource', name: 'wood', amount: 10},
            {type: 'alloy', name: 'steel', amount: 10},
            {type: 'wildResource', name: 'leather', amount: 10},
        ],
        maxOccupancy: 10,
        levelBonus: 1,
        consumption: 20,
        resourceUsed: [
            {type: 'polymer', name: 'glue', amount: 1},
            {type: 'polymer', name: 'rubber', amount: 2},
            {type: 'chemicalSubstance', name: 'water', amount: 3},
        ],
        production: 0.25,
        condition: {plan: 1, type: 'construction', num: 10},
        resourceCreated: {type: 'polymer', name: 'adhesiveResin'},
        description: "Bonne résine sa mère avec un bon goût mais surtout très adhésive."
    },
};

export default {
    constructionData
};