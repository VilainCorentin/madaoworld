let constructionData = {
    0: {
        name: "Cintreuse à acier",
        cost: [
            {type: 'essential', name: 'moka', amount: 10},
            {type: 'mineral', name: 'iron', amount: 10},
            {type: 'wildResource', name: 'wood', amount: 10},
        ],
        maxOccupancy: 8,
        levelBonus: 1,
        consumption: 5,
        resourceUsed: [
            {type: 'mineral', name: 'iron', amount: 3},
            {type: 'mineral', name: 'carbon', amount: 1},
        ],
        production: 1,
        condition: {plan: 1, type: 'construction', num: 0},
        resourceCreated: {type: 'alloy', name: 'steel'},
        description: "C'est facile tu sais, c'est comme faire l'amour : gauche, bas, rotation 62 degrés, engagement rotor."
    },
};

export default {
    constructionData
};