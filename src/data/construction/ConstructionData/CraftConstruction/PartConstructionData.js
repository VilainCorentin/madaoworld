let constructionData = {
    0: {
        name: "Graveur photovoltaïque",
        cost: [
            {type: 'essential', name: 'moka', amount: 10},
            {type: 'alloy', name: 'steel', amount: 10},
            {type: 'polymer', name: 'glue', amount: 10},
        ],
        maxOccupancy: 4,
        levelBonus: 1,
        consumption: 10,
        resourceUsed: [
            {type: 'polymer', name: 'polycarbonate', amount: 3},
            {type: 'polymer', name: 'adhesiveResin', amount: 1},
        ],
        production: 0.5,
        condition: {plan: 1, type: 'construction', num: 13},
        resourceCreated: {type: 'part', name: 'photovoltaicCell'},
        description: "Aziz ! Lumière !"
    },
};

export default {
    constructionData
};