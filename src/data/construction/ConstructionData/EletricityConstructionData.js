let constructionData = {
    0: {
        name: "Pédalier ascensionnel",
        cost: [
            {type: 'essential', name: 'moka', amount: 10},
        ],
        maxOccupancy: 1,
        levelBonus: 1,
        production: 5,
        consumption: 1,
        resourceCreated: {type: 'power', name: 'electricity'},
        description: "Faite pédaler quelqu’un pour générer de l'électricité. La technique n’est pas toute jeune mais ça reste efficace quand on a rien d’autre."
    },
    1: {
        name: "Panneau photovoltaïque",
        cost: [
            {type: 'essential', name: 'moka', amount: 10},
            {type: 'alloy', name: 'steel', amount: 10},
            {type: 'chemicalSubstance', name: 'methane', amount: 10},
            {type: 'part', name: 'photovoltaicCell', amount: 10},
        ],
        maxOccupancy: 1,
        levelBonus: 1,
        production: 500,
        consumption: 1,
        condition: {plan: 1, type: 'construction', num: 13},
        resourceCreated: {type: 'power', name: 'electricity'},
        description: "Unlimited power ! Sauf si vous atterrissez sur une planète appelée ‘Bretagne’."
    },
};

export default {
    constructionData
};