let constructionData = {
    0: {
        name: "Distributeur de pioches numériques ferreux-aimantées",
        cost: [
            {type: 'essential', name: 'moka', amount: 10},
        ],
        maxOccupancy: 5,
        levelBonus: 1,
        production: 1,
        consumption: 1,
        resourceCreated: {type: 'mineral', name: 'iron'},
        description: "Fournit des pioches générées à partir de hash crypté permettant de creuser aléatoirement le sol dans l’espoir d’attirer des minerais de Fer."
    },
    1: {
        name: "Extra-compacteur de roche",
        cost: [
            {type: 'essential', name: 'moka', amount: 10},
            {type: 'mineral', name: 'iron', amount: 10},
            {type: 'wildResource', name: 'wood', amount: 10},
        ],
        maxOccupancy: 6,
        levelBonus: 1,
        consumption: 4,
        production: 0.5,
        condition: {plan: 1, type: 'construction', num: 11},
        resourceCreated: {type: 'mineral', name: 'carbon'},
        description: "Difficile d’obtenir du carbone juste en serrant les fesses."
    },
};

export default {
    constructionData
};