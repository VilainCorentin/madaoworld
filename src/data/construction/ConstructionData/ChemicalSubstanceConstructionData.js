let constructionData = {
    0: {
        name: "Pompe manuelle à Propane",
        cost: [
            {type: 'essential', name: 'moka', amount: 10},
            {type: 'mineral', name: 'iron', amount: 10},
        ],
        maxOccupancy: 1,
        levelBonus: 1,
        production: 1,
        consumption: 1,
        resourceCreated: {type: 'chemicalSubstance', name: 'propane'},
        description: "Insérer, pomper, aspirer. Attention c’est salé."
    },
    1: {
        name: "Méthaniseur à étron",
        cost: [
            {type: 'essential', name: 'moka', amount: 10},
            {type: 'alloy', name: 'steel', amount: 10},
            {type: 'wildResource', name: 'wood', amount: 10},
            {type: 'mineral', name: 'carbon', amount: 10},
        ],
        maxOccupancy: 3,
        levelBonus: 1,
        consumption: 10,
        production: 0.5,
        condition: {plan: 1, type: 'construction', num: 2},
        resourceCreated: {type: 'chemicalSubstance', name: 'methane'},
        description: "Une manière élégante de donner une seconde vie à vos déchets odorants."
    },
    2: {
        name: "Filtre hydrique",
        cost: [
            {type: 'essential', name: 'moka', amount: 10},
            {type: 'wildResource', name: 'wood', amount: 10},
            {type: 'wildResource', name: 'leather', amount: 10},
        ],
        maxOccupancy: 6,
        levelBonus: 1,
        consumption: 10,
        production: 0.5,
        condition: {plan: 1, type: 'construction', num: 3},
        resourceCreated: {type: 'chemicalSubstance', name: 'water'},
        description: "Récupérer l’eau dans l’air… Pourquoi vous n'y avais pas pensé avant !"
    },
    3: {
        name: "Puit à eau",
        cost: [
            {type: 'essential', name: 'moka', amount: 10},
            {type: 'alloy', name: 'steel', amount: 10},
            {type: 'polymer', name: 'polycarbonate', amount: 10},
            {type: 'polymer', name: 'glue', amount: 10},
        ],
        maxOccupancy: 10,
        levelBonus: 1,
        consumption: 20,
        production: 1,
        condition: {plan: 2, type: 'construction', num: 0},
        resourceCreated: {type: 'chemicalSubstance', name: 'water'},
        description: "À force de creuser vous allez forcément trouver quelque chose. Le travailleur n’a pas peur d’avoir de la boue sur le casque."
    },
};

export default {
    constructionData
};