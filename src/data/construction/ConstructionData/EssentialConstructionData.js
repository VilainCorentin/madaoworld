let constructionData = {
    0: {
        name: "Cuisine itinérante",
        cost: [
            {type: 'essential', name: 'moka', amount: 10},
        ],
        maxOccupancy: 10,
        levelBonus: 1,
        production: 2,
        consumption: 1,
        resourceCreated: {type: 'essential', name: 'nutritious_paste'},
        description: "Petit investissement dans des machines de préparation de sandwichs bas de gamme et mauvais pour la santé à base de viande de dindon… mais le peuple aime ça."
    },
    1: {
        name: "Kit de tapis à moka",
        cost: [
            {type: 'essential', name: 'moka', amount: 10},
        ],
        maxOccupancy: 1,
        levelBonus: 1,
        production: 2,
        consumption: 1,
        resourceCreated: {type: 'essential', name: 'moka'},
        description: "Simple tapis avec lequel les autochtones peuvent se rouler par terre afin d’absorber le moka présent au contact du tapis."
    },
    2: {
        name: "Bibliothèque des anciens numérique",
        cost: [
            {type: 'essential', name: 'moka', amount: 10},
        ],
        maxOccupancy: 10,
        consumption: 3,
        levelBonus: 1,
        production: 1,
        resourceCreated: {type: 'essential', name: 'knowledge'},
        description: "Tout le savoir des anciens à découvrir et redécouvrir en famille. Beaucoup de lecture en perspective."
    },
    3: {
        name: "Extracteur terrestre vibratoir",
        cost: [
            {type: 'essential', name: 'moka', amount: 10},
            {type: 'alloy', name: 'steel', amount: 10},
            {type: 'chemicalSubstance', name: 'propane', amount: 10},
        ],
        maxOccupancy: 10,
        consumption: 15,
        levelBonus: 1,
        production: 2,
        condition: {plan: 1, type: 'construction', num: 1},
        resourceCreated: {type: 'essential', name: 'moka'},
        description: "à mettre"
    },
};

export default {
    constructionData
};