
export class ConstructionStorage {

    constructor () {
        this.constructStorage = {
            Vitale: {
                0: [],
                1: [],
                2: [],
                3: [],
            },
            Habitat: {
                0: [],
                1: [],
            },
            Chemical: {
                0: [],
                1: [],
                2: [],
                3: [],
            },
            Electricity: {
                0: [],
                1: [],
            },
            Mineral: {
                0: [],
                1: [],
            },
            WildResource: {
                0: [],
                1: [],
            },
            Alliage: {
                0: [],
            },
            Piece: {
                0: [],
            },
            Polymer: {
                0: [],
                1: [],
                2: [],
                3: [],
                4: [],
            },
        }
    }

    getBuildingsByType(type, num){
        return this.constructStorage[type][num]
    }

    addBuilding(type, num, build){
        this.constructStorage[type][num].push(build)
    }

    getConstruction(type){
        return this.constructStorage[type]
    }

}