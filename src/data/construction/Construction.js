export class Construction {

    constructor(attr) {
        this.occupants = [];
        this.maxOccupancy = attr.maxOccupancy || 0;
        this.cost = attr.cost || {};
        this.name = attr.name;
        this.selectCharge = 0;
        this.condition = attr.condition || {};
        this.level = 1;
        this.levelBonus = attr.consumption || 1;
        this.description = attr.description || '';
        this.overhead = attr.overhead || 2;
        this.xp = 0;
        this.consumption = attr.consumption || 0;
        this.actualConso = 0;
        this.actualProduction = 0;
    }

    expectedConso(){
        return ((this.selectCharge * this.consumption) / 100)
    }

    isFull(){
        return ((this.currentOccupancy + 1) > this.maxOccupancy)
    }

    addOccupant(autochthon) {
        this.occupants.push(autochthon);
    }

    removeOccupant(autochthon) {
        this.occupants = this.occupants.filter(item => item !== autochthon);
    }

    get currentOccupancy() {
        return this.occupants.length;
    }

    getProduction(){}

}