let autochthonCreated = 0;

export class Autochthon {

    constructor(){
        this.id = autochthonCreated;
        this.name = "Rodrigo le poncho"
        this.xp = 0;
        this.level = 1;
        this.isWorking = false;
        this.work = {type: -1, id: -1};
        this.house = {type: -1, id: -1};
        this.job = {}
        autochthonCreated++;
    }

    get nextLevelXp(){
        return 10 * this.level;
    }

}