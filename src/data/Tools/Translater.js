export class Translater {

    constructor(){
        this.dicoTranslate = {
            essential: "Vitale",
            moka: "Moka",
            alloy: "Alliage",
            steel: "Acier",
            wildResource: "Ressource sauvage",
            leather: "Cuir",
            wood: "Bois",
            iron: "Fer",
            propane: "Propane",
            methane: "Methane",
            knowledge: "Connaissance",
            polycarbonate: "Polycarbonate",
            rubber: "Caoutchouc",
            glue: "Colle",
            polyester: "Polyester",
            carbon: "Carbone"
        }
    }

    getTranslate(world){
        if(this.dicoTranslate[world] !== undefined){
            return this.dicoTranslate[world];
        }else{
            return '???'
        }
    }

}