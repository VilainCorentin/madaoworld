export class BindingColor {

    constructor(){
        this.dicoColor = {
            moka: "green accent-4",
            steel: "blue lighten-5",
            leather: "brown lighten-2",
            wood: "brown darken-1",
            iron: "grey darken-1",
            propane: "deep-orange accent-1",
            methane: "deep-orange accent-2",
            knowledge: "cyan accent-3",
            polycarbonate: "yellow darken-4",
            rubber: "lime darken-1",
            glue: "deep-orange darken-3",
            polyester: "deep-orange darken-4"
        }
    }

    getColor(index){
        return this.dicoColor[index];
    }

}