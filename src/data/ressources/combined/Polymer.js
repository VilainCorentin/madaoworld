import {Resource} from "../Resource";

export class Polymer extends Resource{

    constructor() {
        super('Polymer');
        this.elements = {
            polyester: {amount: -1},
            polycarbonate: {amount: -1},
            glue: {amount: -1},
            rubber: {amount: -1},
            adhesiveResin: {amount: -1},
        }
    }


}