import {Resource} from "../Resource";

export class Part extends Resource{

    constructor(){
        super('Part');
        this.elements = {
            photovoltaicCell: {amount: -1},
        }
    }

}