import {Resource} from "../Resource";

export class Alloy extends Resource{

    constructor(){
        super('Alloy');
        this.elements = {
            steel: {amount: -1},
        }
    }
}
