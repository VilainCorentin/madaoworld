import {Electricity} from "../power/Electricity";
import {Mineral} from "./normal/Mineral";
import {Essential} from "./normal/Essential";
import {ChemicalSubstance} from "./normal/ChemicalSubstance";
import {WildResource} from "./normal/WildResource";
import {Alloy} from "./combined/Alloy";
import {Fiber} from "./combined/Fiber";
import {Part} from "./combined/Part";
import {Polymer} from "./combined/Polymer";

export class BindingResources {

    constructor(){
        this.bindingResource = {
            essential: new Essential(),
            mineral: new Mineral(),
            wildResource: new WildResource(),
            chemicalSubstance: new ChemicalSubstance(),
            alloy: new Alloy(),
            polymer: new Polymer(),
            fiber: new Fiber(),
            part: new Part(),
            power: new Electricity(),
        };
    }

    getAmountResource(typeResource, nameResource){
        if (!(typeResource in this.bindingResource)){
            return '?'
        }
        return this.bindingResource[typeResource].getAmountResource(nameResource);
    }

    addAmountResource(typeResource, nameResource, amount){
        this.bindingResource[typeResource].addAmountResource(nameResource, amount)
    }

    removeAmountResource(typeResource, nameResource, amount){
        this.bindingResource[typeResource].removeAmountResource(nameResource, amount)
    }

    addKindOfResource(type, name){
        this.bindingResource[type].elements[name] = {amount: 0}
    }


}