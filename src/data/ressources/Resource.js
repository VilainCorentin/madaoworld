export class Resource {

    constructor(name){
        this.name = name;
        this.elements = {};
    }

    getAmountResource(nameResource){
        if (!(nameResource in this.elements)){
            return '?'
        }
        return this.elements[nameResource].amount;
    }

    addAmountResource(nameResource, amount){
        this.elements[nameResource].amount += amount;
    }

    removeAmountResource(nameResource, amount){
        this.elements[nameResource].amount -= amount;
    }

}