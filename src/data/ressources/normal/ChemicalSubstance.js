import {Resource} from "../Resource";

export class ChemicalSubstance extends Resource{

    constructor(){
        super('Chemical Substance');
        this.elements = {
            propane: {amount: -1},
            methane: {amount: -1},
            water: {amount: -1},
        }
    }

}