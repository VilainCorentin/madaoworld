import {Resource} from "../Resource";

export class WildResource extends Resource{

    constructor(){
        super('Wild Resource');
        this.elements = {
            wood: {amount: -1},
            leather: {amount: -1},
        }
    }

}