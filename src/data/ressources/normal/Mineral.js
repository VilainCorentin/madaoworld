import {Resource} from "../Resource";

export class Mineral extends Resource{

    constructor(){
        super('Mineral');
        this.elements= {
            iron: {amount: -1},
            carbon: {amount: -1},
        }
    }

}