import {Resource} from "../Resource";

export class Essential extends Resource{

    constructor(){
        super('Essential');
        this.elements = {
            moka: {amount: 0},
            nutritious_paste: {amount: -1},
            knowledge: {amount: -1},
        }
    }

}