import {ModuleShip} from "./ModuleShip";
import PlansData from "../improvement/plans/PlansData"

export class ExperimentalLaboratory extends ModuleShip{

    constructor(){
        super();
        this.listPlan = [
            PlansData.plan_0,
            PlansData.plan_1,
            PlansData.plan_2,
        ]
    }

    unlockElemInPlan(indPlan, category, indImpr){
        //TODO: mettre des get au lieu des string static
        this.listPlan[indPlan]['dicoImprovement'][category]['listOfImprovement'][indImpr].unlock = 1
    }

    unlockPlan(indPlan){
        this.listPlan[indPlan].unlock = 1
    }

    isUnlock(indPlan, category, indImpr){
        return this.listPlan[indPlan]['dicoImprovement'][category]['listOfImprovement'][indImpr].unlock
    }

}