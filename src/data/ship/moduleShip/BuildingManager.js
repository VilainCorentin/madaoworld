import {ModuleShip} from "@/data/ship/moduleShip/ModuleShip";
import EssentialConstructionData from "../../construction/ConstructionData/EssentialConstructionData";
import {ConstructionResource} from "../../construction/ConstructionResource";
import HabitationsData from "../../construction/habitation/HabitationsData";
import {Habitation} from "../../construction/habitation/Habitation";
import ChemicalSubstanceConstructionData from "../../construction/ConstructionData/ChemicalSubstanceConstructionData";
import EletricityConstructionData from "../../construction/ConstructionData/EletricityConstructionData";
import MineralConstructionData from "../../construction/ConstructionData/MineralConstructionData";
import WildResourceConstructionData from "../../construction/ConstructionData/WildResourceConstructionData";
import AlloyConstructionData from "../../construction/ConstructionData/CraftConstruction/AlloyConstructionData";
import {ConstructionCraft} from "../../construction/ConstructionCraft";
import PartConstructionData from "../../construction/ConstructionData/CraftConstruction/PartConstructionData";
import PolymerConstructionData from "../../construction/ConstructionData/CraftConstruction/PolymerConstructionData";

export class BuildingManager extends ModuleShip{

    constructor(){
        super();
        this.listResourceBuildData = {
            Vitale: {
                data: EssentialConstructionData,
                f: function (build) {
                    return new ConstructionResource(build)
                },
                c: 'ResourceBuilding'
            },
            Habitat: {
                data: HabitationsData,
                f: function (build) {
                    return new Habitation(build)
                },
                c: 'HabitationBuilding'
            },
            Chemical: {
                data: ChemicalSubstanceConstructionData,
                f: function (build) {
                    return new ConstructionResource(build)
                },
                c: 'ResourceBuilding'
            },
            Electricity: {
                data: EletricityConstructionData,
                f: function (build) {
                    return new ConstructionResource(build)
                },
                c: 'ResourceBuilding'
            },
            Mineral: {
                data: MineralConstructionData,
                f: function (build) {
                    return new ConstructionResource(build)
                },
                c: 'ResourceBuilding'
            },
            WildResource: {
                data: WildResourceConstructionData,
                f: function (build) {
                    return new ConstructionResource(build)
                },
                c: 'ResourceBuilding'
            },
            Alliage: {
                data: AlloyConstructionData,
                f: function (build) {
                    return new ConstructionCraft(build)
                },
                c: 'CraftBuilding'
            },
            Piece: {
                data: PartConstructionData,
                f: function (build) {
                    return new ConstructionCraft(build)
                },
                c: 'CraftBuilding'
            },
            Polymer: {
                data: PolymerConstructionData,
                f: function (build) {
                    return new ConstructionCraft(build)
                },
                c: 'CraftBuilding'
            },
        }
    }

    getDataBuildingFromType(type){
        return this.listResourceBuildData[type]['data'].constructionData
    }

    getConstructionFunctionFromType(type){
        return this.listResourceBuildData[type]['f']
    }

    getConstructionComponantFromType(type){
        return this.listResourceBuildData[type]['c']
    }

}