
export class TerminalCommand{

    constructor() {
        this.isActivated = 0;
    }

    toggleActivation(val){
        this.isActivated = val
    }
}