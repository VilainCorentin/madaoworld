import {TerminalCommand} from "./TerminalCommand";

export class FoodTransponder extends TerminalCommand{

    constructor() {
        super();
        this.name = "Transpondeur Alimentaire";
        this.condition = {plan: 0, type: 'ship', num: 0};
        this.amountResourceUse = 1;
        this.amountResourcecreate = 5;
        this.resourceCreated = {type: 'essential', name: 'nutritious_paste', amount: this.amountResourcecreate};
        this.resouceUse = {type: 'essential', name: 'moka', amount: this.amountResourceUse};
    }
}