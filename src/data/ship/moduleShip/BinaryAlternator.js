import {TerminalCommand} from "./TerminalCommand";

export class BinaryAlternator extends TerminalCommand {

    constructor() {
        super();
        this.name = "Alternateur Binaire";
        this.condition = {plan: 0, type: 'ship', num: 1};
        this.amountResourceUse = 1;
        this.amountResourcecreate = 10;
        this.resourceCreated = {type: 'power', name: 'electricity', amount: this.amountResourcecreate};
        this.resouceUse = {type: 'essential', name: 'moka', amount: this.amountResourceUse};
    }
}