import store from "../../../../store/indexStore"

//TODO: faire la même pour les descritions, les couts, etc comme ça pas besoin d'envoyer ça au serveur

let functionForPlan = {
    0: function () {
        store.commit('addTab', 'Construction')
        store.commit('addConstructionState', {title: "Vitale", icon: "pie_chart_outlined", key: "Vitale"})
        store.commit('addConstructionState', {title: "Habitation", icon: "location_city", key: "Habitat"})
        store.commit('addConstructionState', {title: "Chimique", icon: "hdr_strong", key: "Chemical"})
        store.commit('addConstructionState', {title: "Energie", icon: "flash_on", key: "Electricity"})
        store.commit('addConstructionState', {title: "Minerai", icon: "terrain", key: "Mineral"})
        store.commit('addConstructionState', {title: "Ressource sauvage", icon: "pets", key: "WildResource"})

        //TODO: Faire apparaitre ses tabs plus tard dans le jeu mais pour le moment osef
        store.commit('addConstructionState', {title: "Alliage", icon: "control_point_duplicate", key: "Alliage"})
        store.commit('addConstructionState', {title: "Usinage", icon: "settings_input_composite", key: "Piece"})
        store.commit('addConstructionState', {title: "Polymer", icon: "polymer", key: "Polymer"})

    },
    1: function () {
        store.commit('addTab', 'Terminal')
    },
};

export default {
    functionForPlan
};