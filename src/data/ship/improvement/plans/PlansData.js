import {Plan} from "@/data/ship/improvement/plans/Plan";
import {ShipImprove} from "@/data/ship/improvement/ShipImprove";
import {Improvement} from "@/data/ship/improvement/Improvement";
import {ProdImprove} from "@/data/ship/improvement/ProdImprove";
import {ProdImprovement} from "@/data/ship/improvement/ProdImprovement";
import {ConstructionImprove} from "@/data/ship/improvement/ConstructionImprove";
import {ConstructionImprovement} from "@/data/ship/improvement/ConstructionImprovement";

let dicoImprovement_0 = {
    ship: new ShipImprove({
        0: new Improvement({
            name: "Nettoyage du Transpondeur alimentaire",
            cost: [
                {type: 'essential', name: 'moka', amount: 10}
                ],
        }),
        1: new Improvement({
            name: "Réparation de l'Alternateur binaire",
            cost: [
                {type: 'essential', name: 'moka', amount: 10}
            ]
        }),
        2: new Improvement({
            name: "Déblocage du Gestionnaire des édifications",
            cost: [
                {type: 'essential', name: 'moka', amount: 10}
            ],
            func: 0
        }),
        3: new Improvement({
            name: "Débogage du Terminal des commandes",
            cost: [
                {type: 'essential', name: 'moka', amount: 10}
            ],
            func: 1
        }),
    })
};

let dicoImprovement_1 ={
    ship: new ShipImprove({
        0: new Improvement({
            name: "Reconstruction de la tour de contrôle",
            cost: [
                {type: 'essential', name: 'moka', amount: 10},
                {type: 'alloy', name: 'steel', amount: 10},
                {type: 'wildResource', name: 'leather', amount: 10},
                {type: 'wildResource', name: 'wood', amount: 10},
            ]
        }),
        1: new Improvement({
            name: "Restauration du poste de communication",
            cost: [
                {type: 'essential', name: 'moka', amount: 10},
                {type: 'mineral', name: 'iron', amount: 10},
                {type: 'chemicalSubstance', name: 'propane', amount: 10},
                {type: 'chemicalSubstance', name: 'methane', amount: 10},
            ]
        }),
    }),
    improve: new ProdImprove({
        0: new ProdImprovement({
            name: "Autonettoyant pour cable électrique",
            cost: [
                {type: 'essential', name: 'moka', amount: 10},
                {type: 'mineral', name: 'iron', amount: 10},
            ]
        })
    }),
    construction: new ConstructionImprove({
        0: new ConstructionImprovement({
            name: "Cintreuse à acier",
            category: 'alloy',
            cost: [
                {type: 'essential', name: 'moka', amount: 10},
                {type: 'essential', name: 'knowledge', amount: 10},
                {type: 'mineral', name: 'iron', amount: 10},
            ]
        }),
        1: new ConstructionImprovement({
            name: "Extracteur terrestre vibratoir",
            category: 'essential',
            cost: [
                {type: 'essential', name: 'moka', amount: 10},
                {type: 'essential', name: 'knowledge', amount: 10},
                {type: 'alloy', name: 'steel', amount: 10},
            ]
        }),
        2: new ConstructionImprovement({
            name: "Méthaniseur à étron",
            category: 'chemical substance',
            cost: [
                {type: 'essential', name: 'moka', amount: 10},
                {type: 'essential', name: 'knowledge', amount: 10},
                {type: 'mineral', name: 'iron', amount: 10},
            ]
        }),
        3: new ConstructionImprovement({
            name: "Filtre hydrique",
            category: 'chemical substance',
            cost: [
                {type: 'essential', name: 'moka', amount: 10},
                {type: 'essential', name: 'knowledge', amount: 10},
                {type: 'chemicalSubstance', name: 'methane', amount: 10},
            ]
        }),
        4: new ConstructionImprovement({
            name: "Tube de plantation",
            category: 'wild resource',
            cost: [
                {type: 'essential', name: 'moka', amount: 10},
                {type: 'essential', name: 'knowledge', amount: 10},
                {type: 'chemicalSubstance', name: 'propane', amount: 10},
                {type: 'mineral', name: 'iron', amount: 10},
            ]
        }),
        5: new ConstructionImprovement({
            name: "Cuve de culture cellulaire primaire",
            category: 'wild resource',
            cost: [
                {type: 'essential', name: 'moka', amount: 10},
                {type: 'essential', name: 'knowledge', amount: 10},
                {type: 'chemicalSubstance', name: 'methane', amount: 10},
                {type: 'alloy', name: 'steel', amount: 10},
            ]
        }),
        6: new ConstructionImprovement({
            name: "Presse à granulé polycarbonaté",
            category: 'polymer',
            cost: [
                {type: 'essential', name: 'moka', amount: 10},
                {type: 'essential', name: 'knowledge', amount: 10},
                {type: 'wildResource', name: 'wood', amount: 10},
                {type: 'mineral', name: 'iron', amount: 10},
            ]
        }),
        7: new ConstructionImprovement({
            name: "Laminoire gravitationnel",
            category: 'polymer',
            cost: [
                {type: 'essential', name: 'moka', amount: 10},
                {type: 'essential', name: 'knowledge', amount: 10},
                {type: 'polymer', name: 'polycarbonate', amount: 10},
            ]
        }),
        8: new ConstructionImprovement({
            name: "Mousseuse haute pression",
            category: 'polymer',
            cost: [
                {type: 'essential', name: 'moka', amount: 10},
                {type: 'essential', name: 'knowledge', amount: 10},
                {type: 'polymer', name: 'rubber', amount: 10},
            ]
        }),
        9: new ConstructionImprovement({
            name: "Polymérisateur de colle",
            category: 'polymer',
            cost: [
                {type: 'essential', name: 'moka', amount: 10},
                {type: 'essential', name: 'knowledge', amount: 10},
                {type: 'chemicalSubstance', name: 'methane', amount: 10},
                {type: 'alloy', name: 'steel', amount: 10},
            ]
        }),
        10: new ConstructionImprovement({
            name: "Injecteur à bi-mélange pour adhésif",
            category: 'polymer',
            cost: [
                {type: 'essential', name: 'moka', amount: 10},
                {type: 'essential', name: 'knowledge', amount: 10},
                {type: 'wildResource', name: 'wood', amount: 10},
                {type: 'alloy', name: 'steel', amount: 10},
                {type: 'chemicalSubstance', name: 'propane', amount: 10},
            ]
        }),
        11: new ConstructionImprovement({
            name: "Extra-compacteur de roche",
            category: 'mineral',
            cost: [
                {type: 'essential', name: 'moka', amount: 10},
                {type: 'essential', name: 'knowledge', amount: 10},
                {type: 'mineral', name: 'iron', amount: 10},
            ]
        }),
        12: new ConstructionImprovement({
            name: "Graveur photovoltaïque",
            category: 'part',
            cost: [
                {type: 'essential', name: 'moka', amount: 10},
                {type: 'essential', name: 'knowledge', amount: 10},
                {type: 'chemicalSubstance', name: 'propane', amount: 10},
                {type: 'polymer', name: 'polyester', amount: 10},
            ]
        }),
        13: new ConstructionImprovement({
            name: "Panneau photovoltaïque",
            category: 'eletrcity',
            cost: [
                {type: 'essential', name: 'moka', amount: 10},
                {type: 'essential', name: 'knowledge', amount: 10},
                {type: 'polymer', name: 'glue', amount: 10},
                {type: 'wildResource', name: 'wood', amount: 10},
                {type: 'alloy', name: 'steel', amount: 10},
            ]
        }),
    })
};

let dicoImprovement_2 = {
    improve: new ProdImprove({
        0: new ProdImprovement({
            name: "Compresseur pour cintreuse à acier",
            effect: 10,
            cost: [
                {type: 'essential', name: 'moka', amount: 10},
            ]
        }),
        1: new ProdImprovement({
            name: "L’Anneau unique",
            effect: 100,
            cost: [
                {type: 'essential', name: 'moka', amount: 10},
            ]
        }),
        2: new ProdImprovement({
            name: "Protonator 3000",
            effect: 100,
            cost: [
                {type: 'essential', name: 'moka', amount: 10},
            ]
        }),
        3: new ProdImprovement({
            name: "Coffre fort digital",
            effect: 10000,
            cost: [
                {type: 'essential', name: 'moka', amount: 10},
            ]
        }),
    }),
    construction: new ConstructionImprove({
        0: new ConstructionImprovement({
            name: "Puit à eau",
            category: 'chemical substance',
            cost: [
                {type: 'essential', name: 'moka', amount: 10},
            ]
        }),
    })
};

const plan_0 = new Plan({
    name: "Restauration du vaisseau",
    unlock: 1,
    dicoImprovement: dicoImprovement_0
});

const plan_1 = new Plan({
    name: "Plan de recherche élémentaire",
    cost: [
        {type: 'essential', name: 'knowledge', amount: 10},
        ],
    dicoImprovement: dicoImprovement_1
});

const plan_2 = new Plan({
    name: "Plan de restructuration",
    cost: [
        {type: 'essential', name: 'knowledge', amount: 10},
    ],
    dicoImprovement: dicoImprovement_2
});

export default {
    plan_0,
    plan_1,
    plan_2,
};