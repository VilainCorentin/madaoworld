export class Plan {

    constructor(props){
        this.name = props.name;
        this.unlock = props.unlock || 0;
        this.cost = props.cost || null;
        this.dicoImprovement = props.dicoImprovement;
    }

}