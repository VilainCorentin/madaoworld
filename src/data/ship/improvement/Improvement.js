export class Improvement {

    constructor(props){
        this.name = props.name;
        this.cost = props.cost || 0 ;
        this.unlock = props.unlock || 0;
        this.idFunc = props.func;
    }

}