import {Improve} from "@/data/ship/improvement/Improve";

export class ShipImprove extends Improve{

    constructor(listOfImprovement){
        super(listOfImprovement);
        this.name = 'Vaisseau';
    }

}