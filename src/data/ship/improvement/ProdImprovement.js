import {Improvement} from "@/data/ship/improvement/Improvement";

export class ProdImprovement extends Improvement{

    constructor(props) {
        super(props);
        this.effect = props.effect;
    }


}