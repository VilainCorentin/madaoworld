import {Improve} from "@/data/ship/improvement/Improve";

export class ConstructionImprove extends Improve{

    constructor(listOfImprovement){
        super(listOfImprovement);
        this.name = 'Construction';
    }

}