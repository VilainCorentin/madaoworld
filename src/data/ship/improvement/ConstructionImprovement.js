import {Improvement} from "@/data/ship/improvement/Improvement";

export class ConstructionImprovement extends Improvement{

    constructor(props){
        super(props);
        this.category = props.category;
    }

}