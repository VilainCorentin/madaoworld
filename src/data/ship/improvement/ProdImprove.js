import {Improve} from "@/data/ship/improvement/Improve";

export class ProdImprove extends Improve{

    constructor(listOfImprovement){
        super(listOfImprovement);
        this.name = 'Améliorations';
    }

}