import {Resource} from "../ressources/Resource";

export class Electricity extends Resource{

    constructor(){
        super('Electricity')
        this.elements = {
            electricity: {amount: 0},
        }
    }

}